FROM ruby:3.0 as base
RUN gem install syro redis puma

FROM base as dev
RUN gem install rspec rack-test byebug
EXPOSE 4000

