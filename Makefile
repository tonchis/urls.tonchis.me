.PHONY: test

# We should be able to do `docker-compose exec urls_backend_1 rspec /srv/app_spec.rb`
# to run our test suite inside the dev container, but this sometimes hangs and never finishes.
#
# Instead we're just running a new container with similar env variables.
# The only caveat is that we're mounting the dev.token as an actual file because `docker run`
# doesn't support secrets.
test:
	@docker run -t --rm \
		-e REDIS_URL=redis://redis:6379/15                                \
		-e AUTH_TOKEN_FILE=/run/secrets/auth_token_file                   \
		-v $(shell pwd)/secrets/dev.token:/run/secrets/auth_token_file:ro \
		-v $(shell pwd):/srv                                              \
		--network urls_default                                            \
		--link urls_redis_1                                               \
		urls_backend rspec /srv/app_spec.rb
