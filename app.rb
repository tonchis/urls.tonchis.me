require "syro"

# File.read doesn't let me read a Docker secret /run/secrets file.
auth_token = `cat #{ENV.fetch("AUTH_TOKEN_FILE")}`

App = Syro.new do
  handle(400) do
    res.text("Not a URL")
  end

  post do
    authorization = req.env["HTTP_AUTHORIZATION"]

    unless authorization && authorization == auth_token
      res.status = 403
      finish!
    end

    coerce = ->(input) do
      return false unless input

      url = begin
        URI::parse(input)
      rescue URI::InvalidURIError
        return false
      end

      return false unless url.host

      url.to_s
    end

    url = coerce.(req.params["url"])

    (res.status = 400) && finish! unless url

    res.text Shortener.(url)
  end

  on :slug do
    get do
      url = Shortener.resolve(inbox[:slug])

      if url
        res.redirect(url)
      else
        res.status = 404
      end
    end
  end
end

require "redis"
require "securerandom"

module Shortener
  BASE_URL = "https://urls.tonchis.me"
  EXPIRE   = 3600 * 24 * 30

  module_function

  def call(url)
    redis.multi do
      slug = SecureRandom.hex

      redis.msetnx(url, slug, slug, url)
      redis.expire(url, EXPIRE)
      redis.expire(slug, EXPIRE)
    end

    URI.join(BASE_URL, redis.get(url))
  end

  def resolve(slug)
    redis.get(slug)
  end

  def redis
    @redis ||= Redis.new(url: ENV.fetch("REDIS_URL"))
  end
end

