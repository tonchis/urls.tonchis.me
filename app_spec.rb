require "rspec"
require "rack/test"
require_relative "app"

RSpec.describe App do
  include Rack::Test::Methods

  def app
    @app = App
  end

  def redis
    @redis ||= Redis.new(url: ENV.fetch("REDIS_URL"))
  end

  def auth_token
    `cat #{ENV.fetch("AUTH_TOKEN_FILE")}`
  end

  describe "POST" do
    it "requires authentication" do
      post "/", { url: "foo" }
      expect(last_response.status).to eq(403)
    end

    it "only allows URLs" do
      post "/", { url: "foo" }, { "HTTP_AUTHORIZATION" => auth_token }
      expect(last_response.status).to eq(400)

      post "/", { url: "123" }, { "HTTP_AUTHORIZATION" => auth_token }
      expect(last_response.status).to eq(400)

      post "/", { url: "" }, { "HTTP_AUTHORIZATION" => auth_token }
      expect(last_response.status).to eq(400)

      expect(last_response.body).to eq("Not a URL")
    end

    it "shortens URLs" do
      url = "https://posts.tonchis.me/with/some/path"

      post "/", { url: url }, { "HTTP_AUTHORIZATION" => auth_token }
      expect(last_response.status).to eq(200)

      body = last_response.body
      expect(body).to include("https://urls.tonchis.me")

      slug = URI(body).path[1..-1]
      expect(redis.get(slug)).to eq(url)
    end

    it "is idempotent" do
      url = "https://posts.tonchis.me/with/some/path"

      post "/", { url: url }, { "HTTP_AUTHORIZATION" => auth_token }
      first_response_body = last_response.body
      
      post "/", { url: url }, { "HTTP_AUTHORIZATION" => auth_token }
      expect(last_response.body).to eq(first_response_body)
    end
  end

  describe "GET /:slug" do
    it "redirects to the URL" do
      original_url = "https://posts.tonchis.me/with/some/path"

      post "/", { url: original_url }, { "HTTP_AUTHORIZATION" => auth_token }
      short_url = last_response.body

      get URI(short_url).path
      expect(last_response.status).to   eq(302)
      expect(last_response.location).to eq(original_url)
    end

    it "returns 404 if URL doesn't exist" do
      get "/nope"
      expect(last_response.status).to eq(404)
    end
  end
end

